import gulp from 'gulp';
import pug from 'gulp-pug';
import { Pugs, Public } from './dist';
import { reload } from './server';
import plumber from 'gulp-plumber';
import {stream as wiredep} from 'wiredep';
import errorsHandler from './error';
import bowerConfigs from './bowerConfigs.json';

gulp.task('compilePug', () =>
  gulp.src(Pugs)
    .pipe(plumber({errorHandler: errorsHandler('Pug')}))
    .pipe(pug({
      pretty: true
    }))
    .pipe(wiredep(bowerConfigs))
    .pipe(gulp.dest(Public))
    .pipe(reload())
);

export default gulp.task('pug', ['compilePug'], () =>
  gulp.watch(Pugs, ['compilePug'])
);

export const slimProd = gulp.task('pugProd', ['compilePug']);