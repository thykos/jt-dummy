const express = require('express');
const port = process.env.PORT || 3000;
const app = new express();
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/', (request, response) => {
  response.sendFile(__dirname + '/index.html');
});
app.listen(port, () => console.log(`server opened at port - ${port}`));
